MNIST Digit Recognizer
#########################

These are my notes and scripts used to build various models that can accurately
classify thousands of handwritten images, maintained by `Yann LeCun`_, called the
**M**\odified **N**\ational **I**\nstitute of **S**\tandards and **T**\echnology
(MNIST_) data-set.

.. _MNIST: http://yann.lecun.com/exdb/mnist/
.. _`Yann LeCun`: http://yann.lecun.com/

Notebooks
=========

* **1. - You, Me & Machines, Learning**: Introduce neural networks using the
  Keras_ API by building a simple feed forward neural network to sort the images

* **2. - Neural Networks From Scratch**: A (currently) rough draft of the same
  feed forward neural network as **No.1**, without the use of high level
  frameworks like Keras. Just plain Python and NumPy.
  
.. _keras: https://keras.io/
